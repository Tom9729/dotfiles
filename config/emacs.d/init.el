;;; init --- main configuration file

;;; Commentary:

;;; Code:

(require 'cl)

;; Use a different file for properties set by `customize`.
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file 'noerror)

;; Use Emacs package manager.
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

;; Workaround Windows gnupg issues.
(if (string-equal system-type "windows-nt")
  (setq package-check-signature nil))

;; Install missing packages.
(let ((not-installed (remove-if 'package-installed-p package-selected-packages)))
  (if not-installed
      (progn (package-refresh-contents)
             (package-install-selected-packages))))

;; For libraries and snippets not in package manager.
(add-to-list 'load-path (expand-file-name "~/.emacs.d/lisp"))
(byte-recompile-directory (expand-file-name "~/.emacs.d/lisp") 0)

;; Load theme.
(load-theme 'charcoal-black t t)
(enable-theme 'charcoal-black)

;; Fix default window size on Windows.
(if (string-equal system-type "windows-nt")
    (w32-send-sys-command 61488))

;; Load path from Bash on Linux/Mac.
(if (not (string-equal system-type "windows-nt"))
    (exec-path-from-shell-initialize))

;; Make Putty agent work with Git on Windows.
(if (string-equal system-type "windows-nt")
    (setenv "SSH_AUTH_SOCK" (format "/tmp/.ssh-pageant-%s" (getenv "USERNAME"))))

;; General purpose key-bindings.
(global-set-key "\C-c\C-c" 'comment-or-uncomment-region)
(global-set-key "\C-c\C-a" 'mark-whole-buffer)

;; CTRL+mouse wheel to zoom (Windows/Mac).
(global-set-key [C-wheel-up] 'text-scale-increase)
(global-set-key [C-wheel-down] 'text-scale-decrease)

;; CTRL+mouse wheel to zoom (Linux).
(global-set-key [C-mouse-4] 'text-scale-increase)
(global-set-key [C-mouse-5] 'text-scale-decrease)

;; Enable shift+arrow to jump to another buffer.
(windmove-default-keybindings)
(setq framemove-hook-into-windmove t)

;; M-o to jump to files in project.
(global-set-key (kbd "M-o") #'project-find-file)

;; C indent settings.
(setq c-default-style "bsd"
      c-basic-offset 4)

;; Spaces not tabs!
(setq-default indent-tabs-mode nil)

;; Don't use backup files.
(setq make-backup-files nil
      auto-save-default nil)

;; Scroll output when compiling.
(setq compilation-scroll-output t)

;; Type (y/n) instead of (yes/no)
(fset 'yes-or-no-p 'y-or-n-p)

;; Turn off annoying sound.
(setq ring-bell-function 'ignore)

;; Code folding.
(global-set-key (kbd "C-\"") 'hs-toggle-hiding)
(add-hook 'prog-mode-hook 'hs-minor-mode)

;; Trigger js-mode when json files are loaded.
(add-to-list 'auto-mode-alist '("\\.json\\'" . js-mode))

;; Trigger web-mode for PHP files.
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))

;; Use HLS for Haskell files.
(add-hook 'haskell-mode-hook
          (lambda ()
            (progn
              (eglot-ensure))))

;; Use Clangd for C++ files.
(add-hook 'c++-mode-hook
          (lambda ()
            (progn 
              (eglot-ensure))))
(with-eval-after-load 'eglot
  (add-to-list 'eglot-server-programs
               `((c-mode c++-mode)
                 . ("clangd"
                    ;; Prevent clangd from inserting headers whenever completion performed.
                    "--header-insertion=never"
                    ;; Fix clangd on MacOS.
                    (,@(when (eq system-type 'darwin)
                        "--query-driver=/opt/local/bin/clang++"))))))

;; Make multi-line comments seamless by filling in the * after pressing RET.
(eval-after-load "cc-mode"
  '(define-key c-mode-base-map (kbd "RET") 'c-context-line-break))

;; Make up-down work normally in terminal.
(define-key comint-mode-map (kbd "<up>") 'comint-previous-input)
(define-key comint-mode-map (kbd "<down>") 'comint-next-input)

;; Enable history saving.
(savehist-mode)

;; Enable unique buffer names.
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;; Highlight changed lines for anything under VC.
(require 'diff-hl)
(global-diff-hl-mode)

;; Company mode for auto-completion.
(require 'company)
(add-hook 'after-init-hook 'global-company-mode)
(global-set-key (kbd "M-;") #'company-complete-common)
(setq company-tooltip-align-annotations t)

;; Fix annoying popup windows.
(require 'popwin)
(popwin-mode 1)

;; Make F9 call compile.
(global-set-key [f9] 'compile)

;; Enable Eldoc mode for Elisp.
(add-hook 'emacs-lisp-mode-hook 'turn-on-eldoc-mode)

;; Turn off annoying secondary selection.
(global-unset-key [M-mouse-1])
(global-unset-key [M-drag-mouse-1])
(global-unset-key [M-down-mouse-1])
(global-unset-key [M-mouse-3])
(global-unset-key [M-mouse-2])

;; Fix UTF slowness on Windows.
(when (eq system-type 'nt)
  (setq inhibit-compacting-font-caches t))

;; Remember buffer configuration.
(require 'saveplace)
(require 'desktop+)
(setq desktop-dirname "~/.emacs.d/")
(eyebrowse-mode)

(defun toggle-maximize-window ()
  "Maximize buffer temporarily."
  (interactive)
  (if (= 1 (length (window-list)))
      (jump-to-register '_)
    (progn
      (window-configuration-to-register '_)
      (delete-other-windows))))

(defun eshell-new()
  "Open a new instance of eshell."
  (interactive)
  (eshell 'N))

;; Make Eshell support Bash color codes.
(require 'eshell)
(add-hook 'eshell-before-prompt-hook
          (lambda ()
            (progn
              (setq xterm-color-preserve-properties t)
              (setenv "TERM" "xterm-256color")
              (add-to-list 'eshell-preoutput-filter-functions 'xterm-color-filter)
              (setq eshell-output-filter-functions (remove 'eshell-handle-ansi-color eshell-output-filter-functions))
              )))

(provide 'init)
;;; init.el ends here
