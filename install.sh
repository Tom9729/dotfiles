#!/bin/bash -e
export PATH=/opt/local/libexec/gnubin:$PATH

cd "$(readlink -f $(dirname $0))"

function link_replace {
    local src=$1
    local dest=$2
    local asrc=$(readlink -f $src)
    local old=$dest.old

    if [ -h $dest -a ! -e $dest ]
    then
        echo "removing broken link $dest"
        rm $dest
    fi

    if [ -e $dest -a ! \( -h $dest -a $(readlink -f $dest) = $asrc \) ]
    then
        if [ -e $old ]
        then
            echo "error: file $old already exists" >&2 
            exit 1
        fi        
        echo "renaming $dest to $old"
        mv $dest $old
    fi
    
    if [ ! -e $dest ]
    then
        echo "linking $src to $dest"
        ln -sf $asrc $dest
    fi
}

## Replace config files/directories with links to repo.
link_replace config/emacs.d $HOME/.emacs.d
link_replace config/bashrc $HOME/.bashrc
link_replace config/gitconfig $HOME/.gitconfig
link_replace config/ghci $HOME/.ghci
