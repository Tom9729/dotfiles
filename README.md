# Tom's Dev Setup

My dotfiles configuration for things like Emacs and Bash.

## Install

Run `install.sh` to replace config files with links into this repo
so that configuration will be versioned. This renames existing files/folders
to `name.old` to prevent accidentally clobbering.
